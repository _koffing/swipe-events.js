# Swipe events #

swipe-events.js allows a developer to easily add left, right, up, and down swipe events to their project. This plugin is written in vanilla javascript so no dependencies are required. woop woop!

### Usage ###

* To use just include the swipe-events.js file into your project.

### Example ###

//example of swiping right to change a selected elements background color

swipeRight (element, function(){  
this.style.background = 'blue';  
});