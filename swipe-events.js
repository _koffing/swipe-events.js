'use strict';

//TO DO: add Passive Event Listeners to Improve Scrolling Performance
//TO DO: add polyfill for passive event Listeners

function swipeRight (element, cb){;
    var element = document.querySelectorAll(element);
    return element.forEach(function(item){
        swipe(item, 'right', cb);
    });
}
function swipeLeft(element, cb){
    var element = document.querySelectorAll(element);
    return element.forEach(function(item){
        swipe(item, 'left', cb);
    });
}
function swipeUp(element, cb){
    var element = document.querySelectorAll(element);
    return element.forEach(function(item){
        swipe(item, 'up', cb);
    });
}
function swipeDown(element, cb){
    var element = document.querySelectorAll(element);
    return element.forEach(function(item){
        swipe(item, 'down', cb);
    });
}

function swipe(el, direction, cb){
    var element = el;
    var swipebool;
        var startP;
        var startX;
        var startY;
        var directionModifier;
        var swipeDirection;
        switch(direction){
            case 'right':
                directionModifier = -1;
                swipeDirection = true;
                break;
            case 'left':
                directionModifier = 1;
                swipeDirection = true;
                break;
            case 'down':
                directionModifier = -1;
                swipeDirection = false;
                break;
            case 'up':
                directionModifier = 1;
                swipeDirection = false;
                break;
            default: directionModifier = 1;;
        }
        element.addEventListener("touchstart", function(e){
            swipebool = true;
            startX = e.changedTouches[0].clientX;
            startY = e.changedTouches[0].clientY;
        }, false); 
        element.addEventListener("touchmove", function(e){
            var distanceX = startX - e.changedTouches[0].clientX;
            var distanceY = startY - e.changedTouches[0].clientY;
            if(swipeDirection){
                if(distanceY < 0 ){distanceY *= -1;}
                if (distanceX <= 150 && swipebool && distanceY < (distanceX * directionModifier)){
                    swipebool = false;
                    return cb.call(this, el);
                }
            } else {
                if(distanceX < 0 ){distanceX *= -1;}
                if (distanceY <= 150 && swipebool && distanceX < (distanceY * directionModifier)){
                    swipebool = false;
                    return cb.call(this, el);
                }
            }
            
        }, false); 
}

